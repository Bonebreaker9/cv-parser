package de.qaware;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.fit.pdfdom.PDFDomTree;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

@SpringBootApplication
@EnableTransactionManagement
@EnableWebMvc
public class Application {

    public static void main(String[] args) throws IOException {
        //disable annoying advertisement
        System.getProperties().setProperty("hibernate.types.print.banner", "false");

        SpringApplication.run(Application.class, args);
//
        //Path dir = Path.of("X:\\Test\\input");
        //String destination = "X:\\Test\\output";
        //PDFConverterBA converterBA = new PDFConverterBABoxImpl();
//
        //if (Files.isDirectory(dir)) {
        //    try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(dir)) {
        //        directoryStream.forEach(file -> {
        //            if (file.toString().endsWith(".pdf")) {
        //                converterBA.generateTXTFromPDF(file.toFile(), getDestinationPath(destination, file, ".txt"));
        //            }
        //        });
        //    }
        //}
    }

    private static String getDestinationPath(String destination, Path file, String fileEnding) {
        return destination + "/" + file.getFileName().toString().replace(".pdf", fileEnding);
    }

    private static void generateHTMLFromPDF(File pdfFile, String destination) {
        try (Writer output = new PrintWriter(destination, StandardCharsets.UTF_8)) {
            PDDocument pdf = PDDocument.load(pdfFile);
            new PDFDomTree().writeText(pdf, output);
        } catch (IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
