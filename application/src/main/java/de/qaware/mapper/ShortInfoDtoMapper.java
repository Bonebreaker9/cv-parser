package de.qaware.mapper;

import de.qaware.pdf.ShortPDFInformationDto;
import de.qaware.pdf.dt.ShortPDFInformation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ShortInfoDtoMapper {
    public ShortPDFInformationDto toSource(ShortPDFInformation pdfInformation) {
        return new ShortPDFInformationDto(
                pdfInformation.getId(),
                pdfInformation.getName(),
                pdfInformation.getDate()
        );
    }

    public List<ShortPDFInformationDto> toSources(List<ShortPDFInformation> pdfInformation) {
        return pdfInformation.stream()
                .map(this::toSource)
                .collect(Collectors.toList());
    }
}
