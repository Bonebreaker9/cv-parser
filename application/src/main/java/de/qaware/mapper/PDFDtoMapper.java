package de.qaware.mapper;

import de.qaware.pdf.AttributeDto;
import de.qaware.pdf.PDFDto;
import de.qaware.pdf.dt.PDFInformation;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class PDFDtoMapper {

    public PDFDto toSource(PDFInformation pdfInformation) {
        return new PDFDto(
                pdfInformation.getName(),
                pdfInformation.getAttributeList().stream()
                        .map(attributeET -> new AttributeDto(
                                attributeET.getId(),
                                attributeET.getType(),
                                attributeET.getContent(),
                                attributeET.isCorrect()
                        ))
                        .collect(Collectors.toList())
        );
    }
}
