package de.qaware.rest;

import de.qaware.mapper.PDFDtoMapper;
import de.qaware.mapper.ShortInfoDtoMapper;
import de.qaware.pdf.AttributeChangeDto;
import de.qaware.pdf.PDFDto;
import de.qaware.pdf.PDFManagementBA;
import de.qaware.pdf.ShortPDFInformationDto;
import de.qaware.pdf.dt.AttributeChange;
import de.qaware.pdf.dt.PDFInformation;
import de.qaware.pdf.dt.ShortPDFInformation;
import de.qaware.pdfconverter.PDFConverterBA;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/v1/pdf", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class PDFController {
    private final PDFConverterBA pdfConverterBA;
    private final PDFManagementBA pdfManagementBA;
    private final PDFDtoMapper pdfDtoMapper;
    private final ShortInfoDtoMapper shortInfoDtoMapper;

    /**
     * Import CV pdf and extract as much information as possible from it.
     */
    @PostMapping(path = "/import", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ApiOperation(
            value = "Import CV pdf and extract as much information as possible from it.",
            response = PDFDto.class
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "No content, CV has been imported"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 403, message = "Forbidden, if no authorization was provided")
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void importCV(@RequestParam("file") MultipartFile file) {
        pdfConverterBA.generateTXTFromPDF(file);
    }

    /**
     * Get details for a specific parsed pdf.
     * @return PDFDto
     */
    @GetMapping(path = "/{pdfId}")
    @ApiOperation(
            value = "Get details for a specific parsed pdf.",
            response = PDFDto.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK, result contains all the information about the CV"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 403, message = "Forbidden, if no authorization was provided")
    })
    public PDFDto exportCV(@PathVariable Integer pdfId) {
        PDFInformation pdf = pdfManagementBA.findPDF(pdfId);
        return pdfDtoMapper.toSource(pdf);
    }

    /**
     * Get a specific parsed pdf.
     * @return PDFDto
     */
    @GetMapping(path = "/{pdfId}/doc")
    @ApiOperation(
            value = "Get a specific parsed pdf.",
            response = PDFDto.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK, result contains all the information about the CV"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 403, message = "Forbidden, if no authorization was provided")
    })
    public ResponseEntity<byte[]> exportCVPDF(@PathVariable Integer pdfId) {
        PDFInformation pdf = pdfManagementBA.findPDF(pdfId);
        return ResponseEntity.ok()
                .header("Content-Disposition", "attachment; filename=\""+ pdf.getName()+"\"")
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(pdf.getContent());
    }

    /**
     * Get a overview over all parsed pdf.
     * @return PDFDto
     */
    @GetMapping(path = "/overview")
    @ApiOperation(
            value = "Get a specific parsed pdf.",
            response = PDFDto.class
    )
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK, result contains the information over all parsed pdf"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 403, message = "Forbidden, if no authorization was provided")
    })
    public List<ShortPDFInformationDto> getCVShortInformation() {
        List<ShortPDFInformation> pdfOverview = pdfManagementBA.getPDFOverview();
        return shortInfoDtoMapper.toSources(pdfOverview);
    }

    /**
     * Change a specific attribute.
     */
    @PostMapping(path = "/attribute")
    @ApiOperation(
            value = "Change a specific attribute."
    )
    @ApiResponses({
            @ApiResponse(code = 204, message = "OK"),
            @ApiResponse(code = 401, message = "Unauthorized request"),
            @ApiResponse(code = 403, message = "Forbidden, if no authorization was provided")
    })
    public void changeAttribute(@RequestBody AttributeChangeDto attributeChangeDto) {
        pdfManagementBA.changePDFAttribute(new AttributeChange(attributeChangeDto.getId(),attributeChangeDto.getCorrect()));
    }
}
