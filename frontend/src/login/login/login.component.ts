import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {LoginService} from '../login.service';

@Component({
  selector: 'pdfc-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl()
  });

  constructor(private loginService: LoginService) {
  }

  ngOnInit(): void {
  }

  login(): void {
    this.loginService.login(
      this.loginForm.get('username').value,
      this.loginForm.get('password').value
    );
  }

}
