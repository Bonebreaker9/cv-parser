import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnDestroy {
  baseUrl = 'http://localhost:8080';
  loggedIn = false;

  constructor(private httpClient: HttpClient) {
  }

  login(username: string, password: string): void {
    this.httpClient.post(this.baseUrl + `/login?username=${username}&password${btoa(password)}`,
      {},
      {}
    ).subscribe(_ => this.loggedIn = true);
  }

  logout(): void {
    this.httpClient.post(this.baseUrl + `/logout`,
      {}
    ).subscribe();
  }

  ngOnDestroy(): void {
    if (this.loggedIn) {
      this.logout();
    }
  }
}
