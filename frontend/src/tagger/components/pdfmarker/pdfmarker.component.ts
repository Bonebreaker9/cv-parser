import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {HighlightTag} from 'angular-text-input-highlight';

@Component({
  selector: 'pdfc-pdfmarker',
  templateUrl: './pdfmarker.component.html',
  styleUrls: ['./pdfmarker.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PDFMarkerComponent implements OnInit {
  tags: HighlightTag[] = [{
    indices: {start: 1, end: 4},
    data: {user: {id: 1}}
  }];
  a = 'Test';

  constructor() {
  }

  ngOnInit(): void {

  }

  tagSelected(event: Event) {
    if (event.target instanceof HTMLTextAreaElement) {
      const target = event.target;
      this.addTag(target.selectionStart, target.selectionEnd)
    }
  }

  addTag(start: number, end: number) {
    this.tags = [{
      indices: {start: start, end: end},
      cssClass: 'bg-blue',
      data: {}
    }];
  }
}
