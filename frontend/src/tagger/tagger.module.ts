import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TaggerComponent} from './tagger/tagger.component';
import {MatInputModule} from '@angular/material/input';
import {TextInputHighlightModule} from 'angular-text-input-highlight';
import {FormsModule} from '@angular/forms';
import {PDFMarkerComponent} from './components/pdfmarker/pdfmarker.component';
import {TagControlComponent} from './components/tag-control/tag-control.component';
import {MatButtonModule} from '@angular/material/button';
import {HttpClientModule} from '@angular/common/http';
import {MatGridListModule} from '@angular/material/grid-list';


@NgModule({
  declarations: [TaggerComponent, PDFMarkerComponent, TagControlComponent],
  imports: [
    CommonModule,
    MatInputModule,
    TextInputHighlightModule,
    HttpClientModule,
    MatGridListModule,
    FormsModule,
    MatButtonModule
  ]
})
export class TaggerModule {
}
