import {Injectable, OnDestroy} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {Client, over} from 'stompjs';
import * as SockJS from 'sockjs-client';
import {filter, first, switchMap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {StompSubscription} from '@stomp/stompjs';

@Injectable({
  providedIn: 'root'
})
export class StompService implements OnDestroy {
  private readonly client: Client;
  private state: BehaviorSubject<SocketClientState>;

  constructor() {
    this.client = over(new SockJS(environment.api));
    this.state = new BehaviorSubject<SocketClientState>(SocketClientState.ATTEMPTING);
    this.client.connect({}, () => {
      this.state.next(SocketClientState.CONNECTED);
    });
  }

  private connect(): Observable<Client> {
    return new Observable<Client>(observer => {
      this.state.pipe(
        filter(state => state === SocketClientState.CONNECTED)).subscribe(() => {
          observer.next(this.client);
        }
      );
    });
  }

  onMessage<T>(topic: string): Observable<T> {
    return this.connect().pipe(first(), switchMap(client => {
      return new Observable<T>(observer => {
        const subscription: StompSubscription = client.subscribe(topic, message => {
          observer.next(JSON.parse(message.body));
        });
        return () => client.unsubscribe(subscription.id);
      });
    }));
  }

  send<T>(topic: string, payload: T): void {
    this.connect()
      .pipe(first())
      .subscribe(client => client.send(topic, {}, JSON.stringify(payload)));
  }

  ngOnDestroy(): void {
    this.connect().pipe(first()).subscribe(client => client.disconnect(null));
  }
}

export enum SocketClientState {
  ATTEMPTING, CONNECTED
}
