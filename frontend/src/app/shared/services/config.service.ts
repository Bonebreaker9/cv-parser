import {Injectable} from '@angular/core';
import {CookieService} from 'ngx-cookie';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  constructor(private cookieService: CookieService,
              private httpClient: HttpClient) {
  }
}
