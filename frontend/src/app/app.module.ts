import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from '@angular/router';
import {HeaderComponent} from './header/header.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {LoginComponent} from '../login/login/login.component';
import {CookieModule} from 'ngx-cookie';
import {LoginModule} from '../login/login.module';
import {TaggerComponent} from '../tagger/tagger/tagger.component';
import {TaggerModule} from '../tagger/tagger.module';
import {OverviewComponent} from '../overview/overview/overview.component';
import {OverviewModule} from '../overview/overview.module';
import {ToastrModule} from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  exports: [RouterModule],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppModule.routes, {
      useHash: true
    }),
    CookieModule.forRoot(),
    LoginModule,
    TaggerModule,
    OverviewModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right'
    }), // ToastrModule added
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  static routes = [{
    path: '', children: [
      {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full'
      },
      {
        path: '**',
        redirectTo: '/overview',
        pathMatch: 'full'
      },
      {
        path: 'tagger',
        component: TaggerComponent
      },
      {
        path: 'overview',
        component: OverviewComponent
      },
      {
        path: 'login',
        component: LoginComponent
      }
    ]
  }];
}
