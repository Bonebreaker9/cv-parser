import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ShortPDFInformationDto} from './api/ShortPDFInformationDto';
import {PDFDto} from './api/PDFDto';
import {AttributeChangeDto} from './api/AttributeChangeDto';

@Injectable({
  providedIn: 'root'
})
export class OverviewService {
  url = 'pdfc/api/v1/pdf/';

  constructor(private httpClient: HttpClient) {
  }

  public getPDFOverview(): Promise<ShortPDFInformationDto[]> {

    return this.httpClient.get<ShortPDFInformationDto[]>(this.url + 'overview').toPromise();
  }

  public getPDFDetails(pdfId: number): Promise<PDFDto> {
    return this.httpClient.get<PDFDto>(this.url + pdfId).toPromise();
  }

  public uploadPDF(pdf: FormData) {
    return this.httpClient.post(this.url + 'import',
      pdf,
      {
        responseType: 'arraybuffer',
        observe: 'response'
      }).toPromise();
  }

  public getPDF(pdfId: number) {
    return this.httpClient.get(this.url + pdfId + '/doc',
      {
        responseType: 'arraybuffer',
        observe: 'response'
      }).toPromise();
  }

  public changeAttribute(attributeChangeDto: AttributeChangeDto) {
    console.log(attributeChangeDto);
    return this.httpClient.post<void>(
      this.url + 'attribute',
      attributeChangeDto
    ).toPromise();
  }
}
