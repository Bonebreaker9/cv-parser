export class AttributeDto {
  id: number;
  type: string;
  content: string;
  correct: boolean;
}
