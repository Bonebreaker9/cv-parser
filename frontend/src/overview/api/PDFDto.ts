import {AttributeDto} from './AttributeDto';

export class PDFDto {

  name: string;

  attributeList: AttributeDto[];
}
