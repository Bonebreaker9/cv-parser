import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OverviewComponent} from './overview/overview.component';
import {PdfListComponent} from './components/pdf-list/pdf-list.component';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {HttpClientModule} from '@angular/common/http';
import {PdfViewerComponent} from './components/pdf-viewer/pdf-viewer.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {ToastrModule} from 'ngx-toastr';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';


@NgModule({
  declarations: [OverviewComponent, PdfListComponent, PdfViewerComponent],
    imports: [
        CommonModule,
        MatCardModule,
        MatTableModule,
        HttpClientModule,
        PdfViewerModule,
        MatButtonModule,
        MatIconModule,
        ToastrModule,
        MatPaginatorModule,
        MatSortModule
    ]
})
export class OverviewModule {
}
