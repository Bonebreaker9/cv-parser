import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {OverviewService} from '../../overview.service';
import {PDFDto} from '../../api/PDFDto';
import {MatTableDataSource} from '@angular/material/table';
import {AttributeDto} from '../../api/AttributeDto';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'pdfc-pdf-viewer',
  templateUrl: './pdf-viewer.component.html',
  styleUrls: ['./pdf-viewer.component.css']
})
export class PdfViewerComponent implements OnInit {
  pdfSrc = 'pdfc/api/v1/pdf/1/doc';
  pdfDetails: PDFDto;

  @Input() pdfEvent: ReplaySubject<number> = new ReplaySubject<number>();
  @Input() pdfUploadEvent: ReplaySubject<void> = new ReplaySubject<void>();

  dataSource: MatTableDataSource<AttributeDto>;
  displayedColumns = ['type', 'content', 'correct'];

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private overviewService: OverviewService) {
  }

  ngOnInit(): void {
    this.pdfEvent.subscribe(pdfId => {
      this.pdfSrc = 'pdfc/api/v1/pdf/' + pdfId + '/doc';
      this.overviewService.getPDFDetails(pdfId).then(response => {
        this.pdfDetails = response;

        this.dataSource = new MatTableDataSource(response.attributeList);
        this.dataSource.sort = this.sort;
      });

      this.overviewService.getPDF(pdfId).then(response => {
        this.readPDF(response.body);
      });

    });
  }

  readPDF(pdf: ArrayBuffer): void {

    const blob = new Blob([new Uint8Array(pdf)], {type: 'application/pdf'});

    const objUrl = URL.createObjectURL(blob);
    const iframe = document.getElementById('viewer');
    iframe.setAttribute('src', objUrl);
    URL.revokeObjectURL(objUrl);
  }

  changeAttribute(row: AttributeDto): void  {
    this.overviewService.changeAttribute({
      id: row.id,
      correct: !row.correct
    });
  }
}
