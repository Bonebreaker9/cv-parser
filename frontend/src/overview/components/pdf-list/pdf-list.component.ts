import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {OverviewService} from '../../overview.service';
import {MatTableDataSource} from '@angular/material/table';
import {ShortPDFInformationDto} from '../../api/ShortPDFInformationDto';
import {ReplaySubject} from 'rxjs';
import {ToastrService} from 'ngx-toastr';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'pdfc-pdf-list',
  templateUrl: './pdf-list.component.html',
  styleUrls: ['./pdf-list.component.css']
})
export class PdfListComponent implements OnInit {

  dataSource: MatTableDataSource<ShortPDFInformationDto>;
  displayedColumns = ['id', 'name', 'created'];

  selectedPDF: number;

  @Input() pdfEvent: ReplaySubject<number> = new ReplaySubject<number>();
  @Input() pdfUploadEvent: ReplaySubject<void> = new ReplaySubject<void>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  fileName: string;

  constructor(private overviewService: OverviewService,
              private toastr: ToastrService) {
  }

  ngOnInit(): void {
    this.loadPDFList();
    this.pdfUploadEvent.subscribe(() => {
      this.loadPDFList();
    });
  }

  private loadPDFList(): void {
    this.overviewService.getPDFOverview().then(response => {
      this.dataSource = new MatTableDataSource<ShortPDFInformationDto>();
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.sort.sort({id: 'id', start: 'asc', disableClear: false});
      this.dataSource.data = this.dataSource._orderData(response);
    });
  }

  selectPDF(id: number): void {
    this.selectedPDF = id;
    this.pdfEvent.next(id);
  }

  onFileSelected(event): void {

    const files: File[] = event.target.files;

    for (const file of files) {
      this.fileName = file.name;

      const formData = new FormData();

      formData.append('file', file);

      this.overviewService.uploadPDF(formData).then(() => {
        this.toastr.success('Successfully uploaded');
        this.loadPDFList();
      })
        .catch(error => {
          this.toastr.error('Error while uploading');
          console.log(error);
        });
    }


  }
}
