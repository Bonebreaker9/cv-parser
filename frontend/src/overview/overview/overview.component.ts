import {Component, Input, OnInit} from '@angular/core';
import {ReplaySubject} from 'rxjs';

@Component({
  selector: 'pdfc-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  pdfEvent: ReplaySubject<number> = new ReplaySubject<number>();
  pdfUploadEvent: ReplaySubject<void> = new ReplaySubject<void>();

  constructor() {
  }

  ngOnInit(): void {
  }

}
