package de.qaware.persistence.txt;

import de.qaware.persistence.txt.et.PDFTxtET;
import org.springframework.data.repository.CrudRepository;

/**
 * Repository for PDF texts
 */
public interface TxtRepository extends CrudRepository<PDFTxtET, Integer> {
}
