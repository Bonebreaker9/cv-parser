package de.qaware.persistence.pdf;

import de.qaware.persistence.pdf.et.PDFET;
import de.qaware.persistence.pdf.et.ShortPDFView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Repository for PDFs
 */
public interface PDFRepository extends CrudRepository<PDFET, Integer> {

    @Query(value = "select pdf.id as id, pdf.filename as name, pdf.created as created from PDFET pdf")
    List<ShortPDFView> getAllPDFShortInfos();
}
