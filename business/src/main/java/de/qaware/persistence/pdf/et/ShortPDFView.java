package de.qaware.persistence.pdf.et;

import java.sql.Timestamp;

public interface ShortPDFView {

    Integer getId();
    String getName();
    Timestamp getCreated();
}
