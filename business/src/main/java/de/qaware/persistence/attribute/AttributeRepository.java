package de.qaware.persistence.attribute;

import de.qaware.persistence.attribute.et.AttributeET;
import de.qaware.persistence.pdf.et.PDFET;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Repository for extracted attributes of PDFs
 */
public interface AttributeRepository extends CrudRepository<AttributeET, Integer> {

    List<AttributeET> findAllByPdf(PDFET pdf);
}
