package de.qaware.pdfconverter.impl;

import de.qaware.extractor.BirthdayExtractor;
import de.qaware.extractor.EMailExtractor;
import de.qaware.extractor.NameExtractor;
import de.qaware.pdfconverter.PDFConverterBA;
import de.qaware.persistence.attribute.AttributeRepository;
import de.qaware.persistence.attribute.et.AttributeET;
import de.qaware.persistence.pdf.PDFRepository;
import de.qaware.persistence.pdf.et.PDFET;
import de.qaware.persistence.txt.TxtRepository;
import de.qaware.persistence.txt.et.PDFTxtET;
import de.qaware.persistence.txt.et.PassageET;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;
import org.fit.pdfdom.PDFDomTree;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
@Slf4j
public class PDFConverterBABoxImpl implements PDFConverterBA {
    public static final String Splitting_NL = "\\r?\\n|\\r";
    public static final String Splitting_Passages = "\\s*\\n\\s+";
    public static final String Splitting_Passages_2 = "(\\n\\s*){3,}";
    private final PDFRepository pdfRepository;
    private final TxtRepository txtRepository;
    private final AttributeRepository attributeRepository;
    private final BirthdayExtractor birthdayExtractor;
    private final EMailExtractor eMailExtractor;
    private final NameExtractor nameExtractor;

    @Override
    public void generateHTMLFromPDF(File pdfFile, String destination) {
        try (Writer output = new PrintWriter(destination, StandardCharsets.UTF_8)) {
            PDDocument pdf = PDDocument.load(pdfFile);
            new PDFDomTree().writeText(pdf, output);
        } catch (IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void generateTXTFromPDF(MultipartFile pdfFile) {
        log.info("Try to extract information out of " + pdfFile.getOriginalFilename());
        try (PDDocument document = PDDocument.load(pdfFile.getBytes())) {
            AccessPermission ap = document.getCurrentAccessPermission();
            if (!ap.canExtractContent()) {
                throw new IOException("You do not have permission to extract text from " + pdfFile.getOriginalFilename());
            }

            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setSortByPosition(true);
            String text = stripper.getText(document);

            PDFET pdfet = savePDF(pdfFile);
            PDFTxtET pdfTxtET = savePDFTxt(pdfet, text);
            extractAttributes(pdfTxtET);

            log.info("Successfully extracted information out of " + pdfFile.getOriginalFilename());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void extractAttributes(PDFTxtET pdfTxtET) {
        PDFET pdf = pdfTxtET.getPdf();

        AttributeET birthday = new AttributeET(null, "birthday", birthdayExtractor.extractBirthdayFromPDF(pdfTxtET), true, pdf);
        AttributeET email = new AttributeET(null, "email", eMailExtractor.getEMail(pdfTxtET), true, pdf);
        AttributeET name = new AttributeET(null, "name", nameExtractor.getName(pdfTxtET), true, pdf);

        attributeRepository.save(birthday);
        attributeRepository.save(email);
        attributeRepository.save(name);
    }

    public PDFET savePDF(MultipartFile pdfFile) throws IOException {
        PDFET pdfet = new PDFET(null, pdfFile.getOriginalFilename(), pdfFile.getBytes(), Timestamp.from(Instant.now()));
        return pdfRepository.save(pdfet);
    }

    public PDFTxtET savePDFTxt(PDFET pdfet, String pdfTxt) throws IOException {
        List<String> passageStrings = Arrays.stream(pdfTxt.split(Splitting_NL))
                .filter(line -> !line.isBlank())
                .collect(Collectors.toList());

        ArrayList<PassageET> passageETS = new ArrayList<>();

        for (int i = 0; i < passageStrings.size(); i++) {
            passageETS.add(new PassageET(null, i, passageStrings.get(i)));
        }

        return txtRepository.save(new PDFTxtET(null, pdfet, passageETS));
    }
}
