package de.qaware.pdfconverter;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * BA to convert a PDF into a machine interpretable format.
 */
public interface PDFConverterBA {

    /**
     * Convert the pdf into a html file
     *
     * @param pdfFile     the pdf
     * @param destination where to save the html file
     */
    void generateHTMLFromPDF(File pdfFile, String destination);

    /**
     * Convert the pdf into a single text
     *
     * @param pdfFile the pdf
     */
    void generateTXTFromPDF(MultipartFile pdfFile);
}
