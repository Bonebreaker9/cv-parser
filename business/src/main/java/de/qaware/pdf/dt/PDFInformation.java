package de.qaware.pdf.dt;

import de.qaware.persistence.attribute.et.AttributeET;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class PDFInformation {

    String name;

    byte[] content;

    List<AttributeET> attributeList;
}
