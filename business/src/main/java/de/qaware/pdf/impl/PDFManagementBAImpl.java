package de.qaware.pdf.impl;

import de.qaware.pdf.PDFManagementBA;
import de.qaware.pdf.dt.AttributeChange;
import de.qaware.pdf.dt.PDFInformation;
import de.qaware.pdf.dt.ShortPDFInformation;
import de.qaware.persistence.attribute.AttributeRepository;
import de.qaware.persistence.attribute.et.AttributeET;
import de.qaware.persistence.pdf.PDFRepository;
import de.qaware.persistence.pdf.et.PDFET;
import de.qaware.persistence.txt.TxtRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class PDFManagementBAImpl implements PDFManagementBA {

    private final PDFRepository pdfRepository;
    private final TxtRepository txtRepository;
    private final AttributeRepository attributeRepository;

    @Override
    public PDFInformation findPDF(Integer id) {
        PDFET pdfEt = pdfRepository.findById(id).orElseThrow();
        List<AttributeET> attributeET = attributeRepository.findAllByPdf(pdfEt);

        return new PDFInformation(pdfEt.getFilename(), pdfEt.getContent(), attributeET);
    }

    @Override
    public List<ShortPDFInformation> getPDFOverview() {
        return pdfRepository.getAllPDFShortInfos().stream()
                .map(shortPDFView ->
                        new ShortPDFInformation(
                                shortPDFView.getId(),
                                shortPDFView.getName(),
                                shortPDFView.getCreated()
                                        .toLocalDateTime()
                                        .format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
                        )
                )
                .collect(Collectors.toList());
    }

    @Override
    public void changePDFAttribute(AttributeChange attributeChange) {
        AttributeET attributeET = attributeRepository.findById(attributeChange.getId()).orElseThrow();
        attributeET.setCorrect(attributeChange.getCorrect());

        attributeRepository.save(attributeET);
    }
}
