package de.qaware.pdf;

import de.qaware.pdf.dt.AttributeChange;
import de.qaware.pdf.dt.PDFInformation;
import de.qaware.pdf.dt.ShortPDFInformation;

import java.util.List;

/**
 * Class to manage all PDFs
 */
public interface PDFManagementBA {

    /**
     * Find information about a specific parsed PDF.
     *
     * @param id of the parsed PDF
     * @return detailed information about the PDF
     */
    PDFInformation findPDF(Integer id);

    /**
     * Get an overview over all parsed PDFs.
     *
     * @return an overview over all parsed PDFs
     */
    List<ShortPDFInformation> getPDFOverview();

    /**
     * Change a specific found attribute of a PDF
     *
     * @param attributeChange the information about the change of the attribute
     */
    void changePDFAttribute(AttributeChange attributeChange);
}
