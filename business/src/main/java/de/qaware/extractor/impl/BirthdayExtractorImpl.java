package de.qaware.extractor.impl;

import de.qaware.extractor.BirthdayExtractor;
import de.qaware.persistence.txt.et.PDFTxtET;
import de.qaware.persistence.txt.et.PassageET;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreEntityMention;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.ocpsoft.prettytime.nlp.PrettyTimeParser;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class BirthdayExtractorImpl implements BirthdayExtractor {
    @Override
    public String extractBirthdayFromPDF(PDFTxtET pdfTxtET) {
        String birthdayLine = pdfTxtET.getPassages().stream()
                .sorted(Comparator.comparing(PassageET::getPosition))
                .map(PassageET::getText)
                .filter(this::isBirthdayline)
                .findFirst()
                .orElse("");

        String birthday = getBirthdayWithStanfordNLP(birthdayLine);
        // if (birthday.isBlank()) {
        //     return "";
        // }

        return getGermanDate(birthday, birthdayLine);

        // if (matchResult.isEmpty()) {
        //     return "";
        // }
//
        // String date = matchResult.get().group();
//
        // return getISODate(date);
    }

    private boolean isBirthdayline(String text) {
        return text.toLowerCase().contains("geburt")
                || text.toLowerCase().contains("geboren")
                || text.toLowerCase().matches(".*(geb\\.) [\\w\\.\\\\ ]*\\d{4}.*");
    }

    private String getBirthdayWithStanfordNLP(String birthdayLine) {
        StanfordCoreNLP pipeline = new StanfordCoreNLP();

        CoreDocument coreDocument = new CoreDocument(birthdayLine);

        pipeline.annotate(coreDocument);

        if (coreDocument.entityMentions() == null) {
            return "";
        }
        return coreDocument.entityMentions().stream()
                .filter(coreEntityMention -> coreEntityMention.entityType().contains("DATE"))
                .map(CoreEntityMention::text)
                .findFirst()
                .orElse("");
    }

    private String getBirthdayWithPrettyTime(String birthdayLine) {
        List<Date> dateList = new PrettyTimeParser().parse(birthdayLine);
        if (dateList.isEmpty()) {
            return "";
        }
        return DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL).format(dateList.get(0).toInstant());
    }

    private String getISODate(String date) {
        int year;
        int month = 1;
        int day = 1;

        String[] splittedDate = date.split("[^0-9]");
        year = Integer.parseInt(splittedDate[splittedDate.length - 1]);
        if (year < 100) {
            year += 2000;
            if (LocalDate.now().minusYears(14).isBefore(LocalDate.ofYearDay(year, 1))) {
                year -= 100;
            }
        }

        if (splittedDate.length == 2) {
            month = Integer.parseInt(splittedDate[0]);
        } else if (splittedDate.length == 3) {
            day = Integer.parseInt(splittedDate[0]);
            month = Integer.parseInt(splittedDate[1]);
            if (month > 12) {
                int months = day;
                day = month;
                month = months;
            }
        }

        return LocalDate.of(year, month, day).format(DateTimeFormatter.ISO_DATE);
    }

    private String getGermanDate(String birthday, String birthdayLine) {

        if (birthday.replaceAll("[^0-9]", "").length() >= 6) {
            return birthday;
        }

        Map<String, Integer> monthTexts = Map.ofEntries(
                Map.entry("januar", 1),
                Map.entry("februar", 2),
                Map.entry("märz", 3),
                Map.entry("april", 4),
                Map.entry("mai", 5),
                Map.entry("juni", 6),
                Map.entry("juli", 7),
                Map.entry("august", 8),
                Map.entry("september", 9),
                Map.entry("oktober", 10),
                Map.entry("november", 11),
                Map.entry("dezember", 12)
        );

        String[] birthdayParts = birthdayLine.toLowerCase().replaceAll(" ", " ").split(" ");
        for (int i = 0; i < birthdayParts.length; i++) {
            String birthdayPart = birthdayParts[i].replace(",", "")
                    .replace(";", "");
            if (birthdayPart.length() == 4 && birthdayPart.replaceAll("[0-9]", "").isBlank()) {
                if (i > 2) {
                    Integer month = monthTexts.getOrDefault(birthdayParts[i - 1], 0);
                    if (month != 0) {
                        Integer day = Integer.parseInt(birthdayParts[i - 2].replaceAll("[^0-9]", ""));

                        return addLeadingZero(day) + "." + addLeadingZero(month) + "." + birthdayPart;
                    }
                }

                return birthdayPart;
            }
        }

        return "";
    }

    private String addLeadingZero(Integer number) {
        if (number < 10) {
            return "0" + number;
        }
        return number.toString();
    }
}
