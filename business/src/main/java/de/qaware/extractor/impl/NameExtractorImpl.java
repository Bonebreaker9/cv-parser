package de.qaware.extractor.impl;

import de.qaware.extractor.NameExtractor;
import de.qaware.persistence.txt.et.PDFTxtET;
import de.qaware.persistence.txt.et.PassageET;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreEntityMention;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.stream.Collectors;

@Component
public class NameExtractorImpl implements NameExtractor {
    @Override
    public String getName(PDFTxtET pdfTxtET) {
        // Properties props = new Properties();
        // props.setProperty("annotators", "tokenize,ssplit,pos,lemma,ner");
        // props.setProperty("tokenize.language", "german");

        StanfordCoreNLP pipeline = new StanfordCoreNLP("german");

        String documentText = pdfTxtET.getPassages().stream()
                .sorted(Comparator.comparing(PassageET::getPosition))
                .map(PassageET::getText)
                .collect(Collectors.joining(" und "));

        CoreDocument coreDocument = new CoreDocument(documentText);

        pipeline.annotate(coreDocument);

        if (coreDocument.entityMentions() == null) {
            return "";
        }

        return coreDocument.entityMentions().stream()
                .filter(coreEntityMention -> coreEntityMention.entityType().contains("PERSON"))
                .map(CoreEntityMention::text)
                .filter(name -> !name.toLowerCase().contains("curriculum") && !name.toLowerCase().contains("lebenslauf"))
                .findFirst()
                .orElse("");
    }
}
