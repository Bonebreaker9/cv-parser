package de.qaware.extractor.impl;

import de.qaware.extractor.EMailExtractor;
import de.qaware.persistence.txt.et.PDFTxtET;
import de.qaware.persistence.txt.et.PassageET;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Optional;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;

@Component
public class EMailExtractorImpl implements EMailExtractor {
    @Override
    public String getEMail(PDFTxtET pdfTxtET) {
        String eMailLine = pdfTxtET.getPassages().stream()
                .sorted(Comparator.comparing(PassageET::getPosition))
                .map(PassageET::getText)
                .filter(text -> text.toLowerCase().contains("@"))
                .findFirst()
                .orElse("");

        if (eMailLine.isBlank()) {
            return "";
        }

        // Search for better regex?
        Optional<MatchResult> matchResult = Pattern.compile("[^\\s]+@[^\\s]+")
                .matcher(eMailLine)
                .results()
                .findFirst();
        if (matchResult.isPresent()) {
            return matchResult.get().group();
        }

        return "";
    }
}
