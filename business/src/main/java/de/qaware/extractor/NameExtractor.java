package de.qaware.extractor;

import de.qaware.persistence.txt.et.PDFTxtET;

/**
 * Extractor for name of the applicant
 */
public interface NameExtractor {

    /**
     * Extract the name of the applicant from the pdf text
     *
     * @param pdfTxtET the pdf text
     * @return the name of the applicant
     */
    String getName(PDFTxtET pdfTxtET);
}
