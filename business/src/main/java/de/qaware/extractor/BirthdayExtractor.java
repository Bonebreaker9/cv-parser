package de.qaware.extractor;

import de.qaware.persistence.txt.et.PDFTxtET;

/**
 * Extractor for birthdays
 */
public interface BirthdayExtractor {

    /**
     * Extract the birthday from the pdf text
     *
     * @param pdfTxtET the pdf text
     * @return the birthday
     */
    String extractBirthdayFromPDF(PDFTxtET pdfTxtET);
}
