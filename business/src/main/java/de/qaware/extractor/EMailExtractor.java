package de.qaware.extractor;

import de.qaware.persistence.txt.et.PDFTxtET;

/**
 * Extractor for e-mails
 */
public interface EMailExtractor {

    /**
     * Extract the e-mail from the pdf text
     *
     * @param pdfTxtET the pdf text
     * @return the e-mail
     */
    String getEMail(PDFTxtET pdfTxtET);
}
