create table pdfc_pdf
(
    id       serial  not null
        constraint pdfc_pdf_pk
            primary key,
    filename varchar not null,
    content  bytea,
    created timestamp not null
        default current_timestamp
);

create table pdfc_pdf_txt
(
    id     serial not null
        constraint pdfc_pdf_txt_pk
            primary key,
    pdf_id integer
        constraint pdfc_pdf_txt_pdfc_pdf_id_fk
            references pdfc_pdf
            on delete cascade
);

create table pdfc_passage
(
    id         serial  not null
        constraint pdfc_passage_pk
            primary key,
    position   integer not null,
    text       text    not null,
    pdf_txt_id integer
        constraint pdfc_passage_pdfc_pdf_txt_id_fk
            references pdfc_pdf_txt
            on delete cascade
);

create table pdfc_attribute
(
    id      serial  not null
        constraint pdfc_attribute_pk
            primary key,
    type    text    not null,
    content text    not null,
    pdf_id  integer not null
        constraint pdfc_attribute_pdfc_pdf_id_fk
            references pdfc_pdf (id)
            on delete cascade
);
